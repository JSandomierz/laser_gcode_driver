/*
 * gCodeChunk.c
 *
 *  Created on: Sep 10, 2020
 *      Author: sando
 */

#include "gCodeChunk.h"

GCodeChunk* GCodeChunk__new() {
	GCodeChunk* this = malloc(sizeof(GCodeChunk));
	this->command='\0';
	this->valueAsFloat=0.;
	this->valueAsInt=0;
	return this;
}
void GCodeChunk__delete(GCodeChunk* this) {
	free(this);
}

int GCodeChunk__interpret(GCodeChunk* this, const char* chunk) {
	int status = 0;
	this->command = chunk[0];

	status += sscanf(chunk+1, "%f", &this->valueAsFloat);
	status += sscanf(chunk+1, "%d", &this->valueAsInt);

	if(status == 2) return 0;
	else return status;
}
