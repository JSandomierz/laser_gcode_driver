/*
 * gCodeInterpreter.h
 *
 *  Created on: Sep 9, 2020
 *      Author: sando
 */

#ifndef POSITION_H_
#define POSITION_H_

struct Position_s {
	float x;
	float y;
};

typedef struct Position_s Position;

void Position__set(Position* this, float x, float y);
void Position__setFrom(Position* this, Position* copyFrom);
void Position__add(Position* this, float dx, float dy);
void Position__addPos(Position* this, Position* other);

void Position__print(Position* this);
float Position__distance(Position* this, Position* other);
int Position__equals(Position* this, Position* other, float eta);


#endif /* POSITION_H_ */
