/*
 * gCodeInterpreter.c
 *
 *  Created on: Sep 9, 2020
 *      Author: sando
 */
#include "gCodeInterpreter.h"
#include "gCodeInterpreter_private.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>

//debug
#include <stdio.h>

GCodeInterpreter* GCodeInterpreter__new() {
	GCodeInterpreter* this = malloc(sizeof(GCodeInterpreter));
	Position__set(&this->position, 0., 0.);
	this->isPositioningAbsolute = 1;
	this->motionType = MOTION_NONE;
	return this;
}

void GCodeInterpreter__delete(GCodeInterpreter* this) {
	free(this);
}

int GCodeInterpreter__nextCmd(GCodeInterpreter* this, const char* cmd) {
	printf("GCodeInterpreter__nextCmd: %s\n", cmd);
	char tmp[256];
	GCodeChunk** segments[20];
	memset(segments, 0, 20 * sizeof(char*));

	//strip comments and whitespaces
	char* commentBegin = strchr(cmd, ';');
	if(commentBegin) {
		*commentBegin = '\0';
	}
	//printf("GCodeInterpreter__nextCmd without comment: %s\n", cmd);
	//left whitespaces
	const char* leftTextStart;
	for(leftTextStart = cmd; leftTextStart < cmd+strlen(cmd); ++leftTextStart) {
		if(*leftTextStart != ' ') break;
	}
	//printf("Left text start: %d\n", leftTextStart - cmd);
	//right whitespaces
	const char* rightTextStart;
	for(rightTextStart = cmd+strlen(cmd)-1; rightTextStart >= cmd; --rightTextStart) {
		if(*rightTextStart != ' ') break;
	}
	//printf("Right text start: %d\n", rightTextStart - cmd);

	if(rightTextStart - cmd <= 0) {
		printf("No command, exiting\n");
		return -10;
	}
	unsigned int charsToPut = rightTextStart - leftTextStart + 1;
	//printf("chars to put: %d\n", charsToPut);
	memset(tmp, 0, 50);
	strncpy(tmp, &cmd[leftTextStart - cmd], charsToPut);
	//printf("Clean: %s\n", tmp);
	//separate checksum and check if existing

	char* checksumPtr;
	for(checksumPtr = tmp+strlen(tmp); checksumPtr >= tmp; --checksumPtr) {
		if(*checksumPtr == '*') {
			char checksumVal[3] = {0,0,0};
			strncpy(checksumVal, checksumPtr+1, 2);
			*checksumPtr = '\0';
			//printf("Checksum: %s\n", checksumVal);
			break;
		}
	}
	//printf("No checksum: %s\n", tmp);

	//split segments
	int numSegments = 0;
	char* current;
	char* last;
	char tmpChunk[50];
	for(current = tmp, last = tmp; current < tmp+strlen(tmp)+1; ++current) {
		if(*current == ' ' || *current == '\0') {
			segments[numSegments] = GCodeChunk__new();
			memset(tmpChunk, 0, 50);
			strncpy(tmpChunk, last, current - last);
			GCodeChunk__interpret(segments[numSegments], tmpChunk);
			++numSegments;
			++current;
			last = current;
		}
	}
	GCodeInterpreter__interpretCmd(this, segments, numSegments);
	return 0;
}

void GCodeInterpreter__interpretCmd(GCodeInterpreter* this, GCodeChunk** segments, unsigned int numSegments) {
	int currentSegment = 0;
	if(segments[0]->command == 'N') currentSegment = 1;
//	int i;
//	for(i = currentSegment;i<numSegments;++i) {
//		printf("Segment %d: %c, %4.1f %d\n", i, segments[i]->command, segments[i]->valueAsFloat, segments[i]->valueAsInt);
//	}

	switch (segments[currentSegment]->valueAsInt) {
	case 0:
	case 1:
		if(segments[currentSegment]->command == 'G') {
			GCodeInterpreter__interpretCmdMove(this, segments, numSegments, currentSegment);
		}
		break;
	case 2:
	case 3:
		if(segments[currentSegment]->command == 'G') {
			GCodeInterpreter__interpretCmdMoveArc(this, segments, numSegments, currentSegment);
		}
		break;
	case 4:
		if(segments[currentSegment]->command == 'G') {
			int msecToWait = 0;
			int i;
			for(i=currentSegment; i<numSegments; ++i) {
				if(segments[i]->command == 'P') {
					msecToWait += segments[i]->valueAsInt;
				}
				if(segments[i]->command == 'S') {
					msecToWait += segments[i]->valueAsInt * 1000;
				}
			}
			this->isTimerActive = 0;
			while(this->msecToWait != msecToWait) {//needed because it is shared
				this->msecToWait = msecToWait;
			}
			printf("Set msecToWait at %d\n", this->msecToWait);
			this->isTimerActive = 1;
			this->motionType = MOTION_WAIT;
		}
		break;
	case 28:
		if(segments[currentSegment]->command == 'G') {
			segments[numSegments] = GCodeChunk__new();
			segments[numSegments]->command = 'X';
			segments[numSegments]->valueAsFloat = 0.;
			segments[numSegments]->valueAsInt = 0;
			++numSegments;
			segments[numSegments] = GCodeChunk__new();
			segments[numSegments]->command = 'Y';
			segments[numSegments]->valueAsFloat = 0.;
			segments[numSegments]->valueAsInt = 0;
			++numSegments;
			GCodeInterpreter__interpretCmdMove(this, segments, numSegments, currentSegment);
		}
		break;
	case 90:
		this->isPositioningAbsolute = 1;
		break;
	case 91:
		this->isPositioningAbsolute = 0;
		break;
	default:
		printf("Unknown command code\n");
		break;
	}
	int i;
	for(i=0;i<numSegments;++i) {
		GCodeChunk__delete(segments[i]);
	}
}

/*
 * G0 - rapid move
 * G1 - linear move
 */
void GCodeInterpreter__interpretCmdMove(GCodeInterpreter* this, GCodeChunk** segments, unsigned int numSegments, unsigned int currentSegment) {
	int fastMovement = 1;
	if(segments[currentSegment]->valueAsInt == 1) fastMovement = 0;

	float dx=this->position.x, dy=this->position.y;
	float laserPower = 0.;

	unsigned int i;
	for(i=currentSegment + 1; i < numSegments; ++i) {
		switch(segments[i]->command) {
		case 'X':
			dx = segments[i]->valueAsFloat;
			break;
		case 'Y':
			dy = segments[i]->valueAsFloat;
			break;
		case 'S':
			laserPower = segments[i]->valueAsFloat;
			break;
		default:
			printf("Unknown parameter of command!\n");
			break;
		}
	}

	float deltaAngle = 0.;
	Position linearDelta;
	if(this->isPositioningAbsolute) {
		Position__set(&this->destination, dx, dy);
		Position__setFrom(&linearDelta, &this->position);
		Position__add(&linearDelta, -this->destination.x, -this->destination.y);
		deltaAngle = atan2f(linearDelta.y, linearDelta.x) + PI;
	} else {
	}
	Position__print(&this->position);
	Position__print(&this->destination);

	printf("Delta angle: %5.2f\n", deltaAngle * 180. / PI);
	Position__set(&this->deltaDirection, cosf(deltaAngle), sinf(deltaAngle));
	Position__set(&this->deltaPos, 0., 0.);

	this->motionType = MOTION_LINEAR;
}

/*
 * G2 - cw arc
 * G3 - ccw arc
 */
void GCodeInterpreter__interpretCmdMoveArc(GCodeInterpreter* this, GCodeChunk** segments, unsigned int numSegments, unsigned int currentSegment) {
	float dx=this->position.x, dy=this->position.y;
	float cx=0., cy=0.;
	float laserPower = 0.;

	printf("GCodeInterpreter__interpretCmdMoveArc\n");
	if(segments[currentSegment]->valueAsInt == 2) {
		this->motionType = MOTION_ANGULAR_CW;
		printf("dir -> cw\n");
	} else {
		this->motionType = MOTION_ANGULAR_CCW;
		printf("dir -> ccw\n");
	}

	unsigned int i;
	for(i=currentSegment + 1; i < numSegments; ++i) {
		switch(segments[i]->command) {
		case 'X':
			dx = segments[i]->valueAsFloat;
			break;
		case 'Y':
			dy = segments[i]->valueAsFloat;
			break;
		case 'I':
			cx = segments[i]->valueAsFloat;
			break;
		case 'J':
			cy = segments[i]->valueAsFloat;
			break;
		case 'S':
			laserPower = segments[i]->valueAsFloat;
			break;
		default:
			printf("Unknown parameter of command: '%c'\n", segments[i]->command);
			break;
		}
	}

	Position__set(&this->destination, dx, dy);
	Position__set(&this->rotationCenter, this->position.x, this->position.y);
	Position__add(&this->rotationCenter, cx, cy);
	printf("Dest: %4.2f, %4.2f, this->rotationCenter: %4.2f, %4.2f, laser: %4.2f\n", this->destination.x, this->destination.y, this->rotationCenter.x, this->rotationCenter.y, laserPower);

	this->rotationRadius = Position__distance(&this->position, &this->rotationCenter);
	printf("Rotation radius: %4.2f\n", this->rotationRadius);

	float angleCenterPosition = 0.;
	float angleCenterDestination = 0.;

	float deltaAngle = 0.;
	Position linearDelta;

	Position__setFrom(&linearDelta, &this->rotationCenter);
	Position__add(&linearDelta, -this->position.x, -this->position.y);
	deltaAngle = atan2f(linearDelta.y, linearDelta.x) + PI;
	angleCenterPosition = deltaAngle;
	printf("angle center -> position: %5.2f\n", angleCenterPosition * 180. / PI);

	Position__setFrom(&linearDelta, &this->rotationCenter);
	Position__add(&linearDelta, -this->destination.x, -this->destination.y);
	deltaAngle = atan2f(linearDelta.y, linearDelta.x) + PI;
	angleCenterDestination = deltaAngle;
	printf("angle center -> destination: %5.2f\n", angleCenterDestination * 180. / PI);

	this->rotationAngleCurrent = angleCenterPosition;
	this->rotationAngleDest = angleCenterDestination;
}

int GCodeInterpreter__isCommandFinished(GCodeInterpreter* this) {
	int status = 0;
	if(this->motionType == MOTION_NONE) {
		status = 1;
	}
	return status;
}

int GCodeInterpreter__nextCommandStep(GCodeInterpreter* this) {
	int status = 0;

	//TODO move to device config
	float microstep = 0.01;

	Position nextPos;
	int stepsToMoveX = 0;
	int stepsToMoveY = 0;

	float distance = 0.;
	float nextDistance = distance;

	//linear motion
	if(this->motionType == MOTION_LINEAR) {
		distance = Position__distance(&this->position, &this->destination);
		nextDistance = distance;

		Position__add(&this->deltaPos, this->deltaDirection.x * microstep, this->deltaDirection.y * microstep);
		stepsToMoveX = this->deltaPos.x / microstep;
		stepsToMoveY = this->deltaPos.y / microstep;
		if(stepsToMoveX != 0 || stepsToMoveY != 0) {
			this->deltaPos.x -= stepsToMoveX * microstep;
			this->deltaPos.y -= stepsToMoveY * microstep;

			//check if next position is better
			Position__setFrom(&nextPos, &this->position);
			Position__add(&nextPos, stepsToMoveX * microstep, stepsToMoveY * microstep);
			nextDistance = Position__distance(&nextPos, &this->destination);
			if( nextDistance >= distance ) {
				printf("Done!\n");
				Position__print(&this->position);
				this->motionType = MOTION_NONE;
				status = 1;
				return status;
			}
			distance = nextDistance;

//			printf("Moving steps: %d, %d\n", stepsToMoveX, stepsToMoveY);
			//TODO
			//driver->moveSteps(stepsToMoveX, stepsToMoveY, );

			Position__add(&this->position, stepsToMoveX * microstep, stepsToMoveY * microstep);
		}
	}
	//end linear motion

	//angular motion
	if(this->motionType == MOTION_ANGULAR_CW || this->motionType == MOTION_ANGULAR_CCW) {
		if(this->motionType == MOTION_ANGULAR_CW) {
			nextDistance = this->rotationAngleCurrent - this->rotationAngleDest;
		} else {
			nextDistance = this->rotationAngleDest - this->rotationAngleCurrent;
		}
		if(nextDistance<0.) nextDistance += 2. * PI;
		distance = nextDistance;

		float deltaAngle = 1. * PI / 180;//1 deg
		if(this->motionType == MOTION_ANGULAR_CW) {
			deltaAngle = -deltaAngle;
		}
		this->rotationAngleCurrent += deltaAngle;
		if(this->rotationAngleCurrent > 2. * PI) this->rotationAngleCurrent = 0.;
		if(this->rotationAngleCurrent < 0.) this->rotationAngleCurrent = 2. * PI;
//		printf("Current rotation angle: %5.3f\n", this->rotationAngleCurrent * 180 / PI);

		Position__setFrom(&nextPos, &this->rotationCenter);
		Position__add(&nextPos, this->rotationRadius * cosf(this->rotationAngleCurrent), this->rotationRadius * sinf(this->rotationAngleCurrent));
//		printf("Next ");
//		Position__print(&nextPos);

		Position deltaPos;
		Position__setFrom(&deltaPos, &nextPos);
		Position__add(&deltaPos, -this->position.x, -this->position.y);
		Position__addPos(&this->deltaPos, &deltaPos);

//		printf("Delta ");
//		Position__print(&deltaPos);

		//check if there are steps to make
		stepsToMoveX = this->deltaPos.x / microstep;
		stepsToMoveY = this->deltaPos.y / microstep;
		if(stepsToMoveX != 0 || stepsToMoveY != 0) {
			this->deltaPos.x -= stepsToMoveX * microstep;
			this->deltaPos.y -= stepsToMoveY * microstep;

			//check if next position is better
			if(this->motionType == MOTION_ANGULAR_CW) {
				nextDistance = this->rotationAngleCurrent - this->rotationAngleDest;
			} else {
				nextDistance = this->rotationAngleDest - this->rotationAngleCurrent;
			}
			if(nextDistance<0.) nextDistance += 2. * PI;

//			printf("d %5.3f, n %5.3f\n", distance, nextDistance);
			if( nextDistance >= distance ) {
				printf("Done!\n");
				Position__print(&this->position);
				this->motionType = MOTION_NONE;
				status = 1;
				return status;
			}
			distance = nextDistance;

//			printf("Moving steps: %d, %d\n", stepsToMoveX, stepsToMoveY);
			//TODO
			//driver->moveSteps(stepsToMoveX, stepsToMoveY, );

			Position__add(&this->position, stepsToMoveX * microstep, stepsToMoveY * microstep);
		}
	}
	//end angular motion

	//await
	if(this->motionType == MOTION_WAIT) {
		if(this->msecToWait <= 0) {
			this->isTimerActive = 0;
			this->msecToWait = 0;
			this->motionType = MOTION_NONE;
			printf("Await done!\n");
		}
	}
	//end await

	return status;
}

void GCodeInterpreter__updateTime(GCodeInterpreter* this) {
	if(this->isTimerActive) {
		this->msecToWait--;
	}
}

void GCodeInterpreter__changeCenterCoords(GCodeInterpreter* this, GCodeChunk** segments, unsigned int numSegments, unsigned int currentSegment) {
	Position deltaPos;
	Position__set(&deltaPos, 0., 0.);

	printf("GCodeInterpreter__changeCenterCoords\n");

	unsigned int i;
	for(i=currentSegment + 1; i < numSegments; ++i) {
		switch(segments[i]->command) {
		case 'X':
			deltaPos.x = -segments[i]->valueAsFloat;
			break;
		case 'Y':
			deltaPos.y = -segments[i]->valueAsFloat;
			break;
		default:
			printf("Unknown parameter of command: '%c'\n", segments[i]->command);
			break;
		}
	}

//	Position__addPos(&this->position, &deltaPos);
	//same for boundaries
	//TODO boundaries
//	Position__addPos(&this->boundary_lx_ly, &deltaPos);
//	Position__addPos(&this->boundary_ux_uy, &deltaPos);
//	Position__addPos(&this->boundary_lx_uy, &deltaPos);
//	Position__addPos(&this->boundary_ux_ly, &deltaPos);
//	printf("Center changed, now at ");
//	Position__print(&this->position);
	printf("Not supported!\n");
}
