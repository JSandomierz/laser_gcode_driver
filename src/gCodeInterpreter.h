/*
 * gCodeInterpreter.h
 *
 *  Created on: Sep 9, 2020
 *      Author: sando
 */

#ifndef GCODEINTERPRETER_H_
#define GCODEINTERPRETER_H_

#include "position.h"
#include "gCodeChunk.h"

enum MOTION_TYPE {
	MOTION_NONE,
	MOTION_LINEAR,
	MOTION_ANGULAR_CW,
	MOTION_ANGULAR_CCW,
	MOTION_WAIT
};

#define ETA 0.00001
#define PI 3.14159265

struct GCodeInterpreter_s {
	Position position;
	int isPositioningAbsolute;//1 - absolute 0 - relative

	enum MOTION_TYPE motionType;
	Position destination;

	Position deltaDirection;

	//linear motion
	Position deltaPos;

	//rotation motion
	float rotationRadius;
	float rotationAngleCurrent;
	float rotationAngleDest;
	Position rotationCenter;

	//await
	char isTimerActive;
	int msecToWait;
};

typedef struct GCodeInterpreter_s GCodeInterpreter;

GCodeInterpreter* GCodeInterpreter__new();
void GCodeInterpreter__delete(GCodeInterpreter* this);
int GCodeInterpreter__nextCmd(GCodeInterpreter* this, const char* cmd);

int GCodeInterpreter__isCommandFinished(GCodeInterpreter* this);
int GCodeInterpreter__nextCommandStep(GCodeInterpreter* this);

void GCodeInterpreter__updateTime(GCodeInterpreter* this);

#endif /* GCODEINTERPRETER_H_ */
