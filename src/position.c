/*
 * gCodeInterpreter.c
 *
 *  Created on: Sep 9, 2020
 *      Author: sando
 */
#include "position.h"

void Position__add(Position* this, float dx, float dy) {
	this->x += dx;
	this->y += dy;
}

void Position__set(Position* this, float x, float y) {
	this->x = x;
	this->y = y;
}
void Position__setFrom(Position* this, Position* copyFrom) {
	this->x = copyFrom->x;
	this->y = copyFrom->y;
}

void Position__addPos(Position* this, Position* other) {
	this->x += other->x;
	this->y += other->y;
}
void Position__print(Position* this) {
	printf("Pos x: %4.2f y: %4.2f\n", this->x, this->y);
}
float Position__distance(Position* this, Position* other) {
	float result = sqrtf((this->x - other->x) * (this->x - other->x)
		+(this->y - other->y) * (this->y - other->y));
	return result;
}

int Position__equals(Position* this, Position* other, float eta) {
	Position diff;
	Position__setFrom(&diff, this);
	Position__add(&diff, -other->x, -other->y);
	if( fabsf(diff.x) < eta && fabsf(diff.y) < eta ) return 1;
	return 0;
}
