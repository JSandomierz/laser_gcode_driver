/*
 * gCodeInterpreter_private.h
 *
 *  Created on: Sep 10, 2020
 *      Author: sando
 */

#ifndef GCODEINTERPRETER_PRIVATE_H_
#define GCODEINTERPRETER_PRIVATE_H_

#include "gCodeInterpreter.h"
#include "gCodeChunk.h"

void GCodeInterpreter__interpretCmd(GCodeInterpreter* this, GCodeChunk** segments, unsigned int numSegments);

/*
 * G0 - rapid move
 * G1 - linear move
 */
void GCodeInterpreter__interpretCmdMove(GCodeInterpreter* this, GCodeChunk** segments, unsigned int numSegments, unsigned int current);

/*
 * G2 - cw arc
 * G3 - ccw arc
 */
void GCodeInterpreter__interpretCmdMoveArc(GCodeInterpreter* this, GCodeChunk** segments, unsigned int numSegments, unsigned int current);

/*
 * G92 - move center to new X/Y
 * translates known positions according to new offset
 */
void GCodeInterpreter__changeCenterCoords(GCodeInterpreter* this, GCodeChunk** segments, unsigned int numSegments, unsigned int current);

#endif /* GCODEINTERPRETER_PRIVATE_H_ */
