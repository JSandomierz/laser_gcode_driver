/*
 ============================================================================
 Name        : laser_gcode_driver.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "gCodeInterpreter.h"

#define NUM_PROGRAM_LINES 12

int main(void) {
	GCodeInterpreter* gci = GCodeInterpreter__new();
	printf("gci %u: %f\n", gci, gci->position.x);
	if(!GCodeInterpreter__isCommandFinished(gci)) {
		printf("ERR\n");
	}

	char program[NUM_PROGRAM_LINES][100] = {
		"N3 T0*57 ; This is a comment",
		"N4 G92 E0*67",
		"; So is this",
		"N5 G28*22",
		"G0 X-5 Y15",
		"G28",
		"G4 P10 S1",
		"G00 X.5 Y3.5 Z.5",
		"G01 Z-.1250 F20",
        "G01 X2.25",
		//"G02 X2.75 Y3 I0 J-.5"
		"G02 X2.25 Y2.5 I0 J-.5",
		"G02 X2.25 Y3.5 I0 J.5",
	};
	int i, j;
	char cmdFinished = 0;
	for(i=0;i<NUM_PROGRAM_LINES;++i) {
		cmdFinished = 0;
		GCodeInterpreter__nextCmd(gci, program[i]);
		for(j=0;j<200000;++j) {

			//updateTime should be called by timer event
			GCodeInterpreter__updateTime(gci);

			if(!GCodeInterpreter__isCommandFinished(gci)) {
				GCodeInterpreter__nextCommandStep(gci);
			} else {
				cmdFinished = 1;
				break;
			}
		}
		if(!cmdFinished) {
			printf("Command not finished at %d!\n", i);
			return 0;
		}
	}

	GCodeInterpreter__delete(gci);
	return EXIT_SUCCESS;
}
