/*
 * gCodeChunk.h
 *
 *  Created on: Sep 10, 2020
 *      Author: sando
 */

#ifndef GCODECHUNK_H_
#define GCODECHUNK_H_

struct GCodeChunk_s {
	char command;
	float valueAsFloat;
	int valueAsInt;
};

typedef struct GCodeChunk_s GCodeChunk;

GCodeChunk* GCodeChunk__new();
void GCodeChunk__delete(GCodeChunk* this);

int GCodeChunk__interpret(GCodeChunk* this, const char* chunk);

#endif /* GCODECHUNK_H_ */
